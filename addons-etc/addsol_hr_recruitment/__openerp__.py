{
    'name' : 'Addsol-HR-Recruitment',
    'summary': 'Human Resources Recruitment',
    'description' : """
Addition IT Solutions Pvt. Ltd.
=================================
    Contact:
    * website: www.aitspl.com
    * email: info@aitspl.com
    
Features:
---------
    *Modification on Job Application Form & Applications
""",

    'author' : 'Addition IT Solutions Pvt. Ltd.',
    
    'category' : 'Addsol mods',
    'version' : '1.0',
    
    'depends' : [
                 'hr_recruitment',
		 'report',
                 ],
    
    'data': [
        'addsol_hr_recruitment_view.xml',
	'sequence.xml',
	'addsol_report_offer.xml',
	'view/report_offer_view.xml',
	'view/report_appoint_view.xml',
	'view/recruit_cof_report.xml',
    ],
    'demo': [],
    'test': [],
    'installable': True,
    'auto_install': False,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
