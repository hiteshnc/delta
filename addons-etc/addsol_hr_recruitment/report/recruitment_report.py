from openerp import api, models
from openerp.exceptions import ValidationError
   
class RecruitmentReport(models.AbstractModel):
    _name = 'report.addsol_hr_recruitment.recruitment_report'
    

    @api.multi
    def render_html(self, data=None):
        report_obj = self.env['report']
        report = report_obj._get_report_from_name('addsol_hr_recruitment.recruitment_report')
        docargs = {
            'doc_ids': self._ids,
            'doc_model': report.model,
            'docs': self,
            'rec_ids':self.env['hr.applicant'].search([('id','in',self.ids)])
    
        }

        return report_obj.render('addsol_hr_recruitment.recruitment_report', docargs)
    




 
