from openerp import _, api, fields, models

class applicant_apl(models.Model):
    
    _inherit = "hr.applicant"
    
    ref_no = fields.Char("Ref No.", readonly = 'True')
    cost_of_recruitment = fields.Float(string = "Cost of Recruitment")
    reason = fields.Text("Reasons for Approval/Rejection")
    gender = fields.Selection([('male' , 'Male'), ('Female','Female')])
    job_location_id = fields.Many2one('addsol.region.state', 'Job Location', ondelete='restrict')
    button_flag = fields.Boolean("Button Hide Flag")
    button_f = fields.Boolean("Button Hidden Flag")
    band = fields.Integer("Band")
    address = fields.Char("Address")
    date_offer = fields.Date("Date of Issuing Offer Letter")
    date_appoint = fields.Date("Date of Issuing Appointment Letter")   

    _defaults = {
     'button_flag' : False,
     'button_f' : False,
     }

    @api.model
    def create(self,vals):
        vals['ref_no'] = self.env['ir.sequence'].next_by_code('hr.applicant.sequence')
        return super(applicant_apl,self).create(vals)
        

    @api.multi
    def approve_applicant(self):
        
        mail_mail_obj = self.env['mail.mail']
        values = {
                'model': None,
                'res_id': None,
                'subject': 'Candidate Approved',
                'body': '''Kind attention: HR manager <br/>  Following candidate has been approved by %s <br/> The details of the candidate are as follows: <br/>Name: %s<br/>Applied Job: %s<br/>Department: %s<br/>Job Location: %s''' %(self.user_id.name, self.partner_name, self.job_id.name, self.department_id.name, self.job_location_id.name), 
                'body_html': '''Kind attention: HR manager <br/>  Following candidate has been approved by %s <br/> The details of the candidate are as follows: <br/>Name: %s<br/>Applied Job: %s<br/>Department: %s<br/>Job Location: %s''' %(self.user_id.name, self.partner_name, self.job_id.name, self.department_id.name, self.job_location_id.name),

                'parent_id': None,
                'attachment_ids': None,
                'email_to': 'dheeraj.agarwal@deltaspharma.com',
                'email_from': 'deltaspharma@gmail.com',
                'auto_delete': True,
            }
        mail_id = mail_mail_obj.create(values)
        mail_mail_obj.send()
        self.write({'button_flag': True})
        return True

    @api.multi
    def reject_applicant(self):
        mail_mail_obj = self.env['mail.mail']
        values = {
                'model': None,
                'res_id': None,
                'subject': 'Candidate Rejected',
                'body': '''Kind attention: HR manager <br/>  Following candidate has been rejected by %s <br/> The details of the candidate are as follows: <br/>Name: %s<br/>Applied Job: %s<br/>Department: %s<br/>Job Location: %s''' %(self.user_id.name, self.partner_name, self.job_id.name, self.department_id.name, self.job_location_id.name),
                'body_html': '''Kind attention: HR manager <br/>  Following candidate has been rejected by %s <br/> The details of the candidate are as follows: <br/>Name: %s<br/>Applied Job: %s<br/>Department: %s<br/>Job Location: %s''' %(self.user_id.name, self.partner_name, self.job_id.name, self.department_id.name, self.job_location_id.name),
                'parent_id': None,
                'attachment_ids': None,
                'email_to': 'dheeraj.agarwal@deltaspharma.com',
                'email_from': 'deltaspharma@gmail.com',
                'auto_delete': True,
            }
        mail_id = mail_mail_obj.create(values)
        mail_mail_obj.send()
        self.write({'button_flag': True})
        return True

    @api.multi
    def assessment_done(self):
        mail_mail_obj = self.env['mail.mail']
        values = {
                'model': None,
                'res_id': None,
                'subject': 'Candidate Assessment Done',
                'body': '''Kind attention: HR manager <br/>  Following candidate has been assessed by %s <br/> The details of the candidate are as follows: <br/>Name: %s<br/>Applied Job: %s<br/>Department: %s<br/>Job Location: %s''' %(self.user_id.name, self.partner_name, self.job_id.name, self.department_id.name, self.job_location_id.name),
                'body_html': '''Kind attention: HR manager <br/>  Following candidate has been assessed by %s <br/> The details of the candidate are as follows: <br/>Name: %s<br/>Applied Job: %s<br/>Department: %s<br/>Job Location: %s''' %(self.user_id.name, self.partner_name, self.job_id.name, self.department_id.name, self.job_location_id.name),
                'parent_id': None,
                'attachment_ids': None,
                'email_to': 'dheeraj.agarwal@deltaspharma.com',
                'email_from': 'deltaspharma@gmail.com',
                'auto_delete': True,
            }
        mail_id = mail_mail_obj.create(values)
        mail_mail_obj.send()
        self.write({'button_f': True})
        return True
    
class hr_job(models.Model):
    
    _inherit = "hr.job"
    
    
    
    
