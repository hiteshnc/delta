# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2015-2016 Addition IT Solutions Pvt. Ltd. (<http://www.aitspl.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models, fields, api, _
from openerp.tools import float_is_zero, float_compare


class invoice_tally(models.Model):
    # Inherits user for add it on addsol.goals
    _inherit = 'account.invoice'

    @api.one
    @api.depends('invoice_line_ids.price_subtotal', 'tax_line_ids.amount', 'currency_id', 'company_id')
    def _compute_amount(self):
        self.amount_untaxed = sum(line.price_subtotal for line in self.invoice_line_ids)
        self.amount_tax = sum(line.amount for line in self.tax_line_ids)
        self.amount_total = self.amount_untaxed + self.amount_tax
        if int(self.amount_total) > 0:
            amounttotaldiff = self.amount_total - int(self.amount_total)
            taxdetail = self.amount_tax - int(self.amount_tax)
            c = 1 - amounttotaldiff
            e = 1 - taxdetail
            if (amounttotaldiff < 0.50):
                d = self.amount_total - amounttotaldiff
                self.round_off_amt_total = - amounttotaldiff
            if (amounttotaldiff >= 0.50):
                d = self.amount_total + c
                self.round_off_amt_total = c

            self.amount_total = d

        amount_total_company_signed = self.amount_total
        amount_untaxed_signed = self.amount_untaxed
        if self.currency_id and self.currency_id != self.company_id.currency_id:
            amount_total_company_signed = self.currency_id.compute(self.amount_total, self.company_id.currency_id)
            amount_untaxed_signed = self.currency_id.compute(self.amount_untaxed, self.company_id.currency_id)
        sign = self.type in ['in_refund', 'out_refund'] and -1 or 1
        self.amount_total_company_signed = amount_total_company_signed * sign
        self.amount_total_signed = self.amount_total * sign
        self.amount_untaxed_signed = amount_untaxed_signed * sign


    @api.one
    @api.depends(
        'state', 'currency_id', 'invoice_line_ids.price_subtotal',
        'move_id.line_ids.amount_residual',
        'move_id.line_ids.currency_id')
    def _compute_residual(self):
        residual = 0.0
        residual_company_signed = 0.0
        sign = self.type in ['in_refund', 'out_refund'] and -1 or 1
        for line in self.sudo().move_id.line_ids:
            if line.account_id.internal_type in ('receivable', 'payable'):
                residual_company_signed += line.amount_residual
                if line.currency_id == self.currency_id:
                    residual += line.amount_residual_currency if line.currency_id else line.amount_residual
                else:
                    from_currency = (line.currency_id and line.currency_id.with_context(date=line.date)) or line.company_id.currency_id.with_context(date=line.date)
                    residual += from_currency.compute(line.amount_residual, self.currency_id)
        #self.residual_company_signed = abs(residual_company_signed) * sign
        if int(residual) > 0:
            amounttotaldiff = residual - int(residual)
            c = 1 - amounttotaldiff
            if (amounttotaldiff < 0.50):
                d = residual - amounttotaldiff
                self.round_off_amt_residual = - amounttotaldiff
            if (amounttotaldiff > 0.50):
                d = residual + c
                self.round_off_amt_residual = c
            residual = d
        self.residual_company_signed = abs(residual) * sign
        self.residual_signed = abs(residual) * sign
        self.residual = abs(residual)
        digits_rounding_precision = self.currency_id.rounding
        if float_is_zero(self.residual, precision_rounding=digits_rounding_precision):
            self.reconciled = True
        else:
            self.reconciled = False

    invoice_tally_no = fields.Char(" Tally Invoice No", help="Tally Invoice Number")
    round_off_amt_total = fields.Monetary(string='Round off', store=True, readonly=True, compute='_compute_amount')
    round_off_amt_residual = fields.Monetary(string='Round off', store=True, readonly=True, compute='_compute_residual')


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
