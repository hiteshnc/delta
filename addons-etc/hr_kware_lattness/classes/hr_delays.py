import time
import pytz
from datetime import datetime,timedelta
from openerp.osv import fields, osv
from openerp import SUPERUSER_ID

class hr_delay_structure(osv.Model):
    _name = "hr.delay.structure"
    _description = "Delay Structure"
    
    _columns = {
        'name':fields.char(string="Name", required=True),
        'warning_count': fields.integer(string="Warnings Counts", required=True),
        
        'delay_rule_ids':fields.many2many('hr.delay.rule', 'hr_delay_structure_hr_delay_rule_rel', 'delay_rule_id', 'delay_structure_id', string='Delay Rules'),
    }
    
    _defaults = {
        'warning_count': 1,
    }
    
class hr_delay_rule(osv.Model):
    _name = "hr.delay.rule"
    _description = "Delay Rules"
    _order = "sequence"
    
    _columns = {
        'name': fields.char(string="Name", required=True),
        'sequence': fields.integer(string='Sequence', required=True),
        'delay_period': fields.float(string="Delay Period", required=True, help="This rule will be applied within the defined period (in minutes)."),
        'exempt_minutes': fields.float(string="Exempted Minutes", required=True),
        'calc_method': fields.selection([
            ('work', 'Calculate from working schedule'),
            ('exempted', 'Calculate after exempted minutes'),
        ], string="Calculation Method", required=True),
        
        'delay_rule_line_ids': fields.one2many('hr.delay.rule.line', 'delay_rule_id', string='Delay Rules Ref.', ondelete="cascade"),
    }
    
    _defaults = {
        'sequence': 1,
        'calc_method': 'work',
    }
    
class hr_delay_rule_line(osv.Model):
    _name = "hr.delay.rule.line"
    _description = "Delay Rule Lines"
    _order = "sequence"
    
    _columns = {
        'name': fields.char(string="Name", required=True),
        'sequence': fields.integer(string="Sequence", required=True),
        'iteration': fields.integer(string="Iteration", required=True),
        'computation_type': fields.selection([
            ('fixed', 'Fixed'), 
            ('rate', 'Rate'),
        ],string='Computation Type', required=True),
        'amount': fields.float(string="Amount / Rate in hour"),
        
        'delay_rule_id': fields.many2one('hr.delay.rule', string='Delay Rules Ref.'),
    }
    _defaults = {
        'sequence': 1,
        'iteration': 1,
        'computation_type': 'fixed',
    }
    
class hr_payroll(osv.Model):
    _inherit = 'hr.payslip'
    
    def get_worked_day_lines(self, cr, uid, contract_ids, date_from, date_to, context=None):
        res = super(hr_payroll, self).get_worked_day_lines(cr, uid, contract_ids, date_from, date_to, context=context)
        
        # Objects definitions
        user_pool = self.pool.get('res.users')
        contract_obj = self.pool.get('hr.contract')
        attendance_obj = self.pool.get('hr.attendance')
        working_hours_obj = self.pool.get('resource.calendar')
        holidays_obj = self.pool.get('hr.holidays')
        # Common Variable
        DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
        TIME_FORMAT = "%H:%M:%S"
        
        def is_in_working_schedule(date_in, working_hours_id):
            found = False
            if type(date_in) is datetime:
                working_hours = working_hours_obj.browse(cr, SUPERUSER_ID, working_hours_id, context=context)
                for line in working_hours.attendance_ids:
                    if int(line.dayofweek) == date_in.weekday():
                        found = True
                        break
            return found
            
        def get_start_hour_of_the_day(date_in, working_hours_id):
            hour = 0.0
            if type(date_in) is datetime:
                working_hours = working_hours_obj.browse(cr, SUPERUSER_ID, working_hours_id, context=context)
                for line in working_hours.attendance_ids:
                    # First assign to hour
                    if int(line.dayofweek) == date_in.weekday() and hour == 0.0:
                        hour = line.hour_from
                    # Other assignments to hour
                    # No need for this part but it's a fail safe condition
                    elif int(line.dayofweek) == date_in.weekday() and hour != 0.0 and line.hour_from < hour:
                        hour = line.hour_from
            return hour
        
        def get_time_from_float(float_time):
            str_time = str(float_time)
            str_hour = str_time.split('.')[0]
            str_minute = ("%2d" % int(str(float("0." + str_time.split('.')[1]) * 60).split('.')[0])).replace(' ','0')
            str_ret_time = str_hour + ":" + str_minute + ":00"
            str_ret_time = datetime.strptime(str_ret_time, TIME_FORMAT).time()
            return str_ret_time

        def _get_float_from_time(td):
            return td.total_seconds()/60/60   
            #return (td.days+(td.seconds // 3600)+(td.seconds % 3600 / 60.0))
            # signOnP = [int(n) for n in time_type.split(":")]
            # signOnH = signOnP[0] + signOnP[1]/60.0
            # return signOnH
        '''
        def _get_diff_time(end_time, start_time):
            tdelta = datetime.strptime(end_time, TIME_FORMAT) - datetime.strptime(start_time, TIME_FORMAT)
            return tdelta
        
        def _get_minutes(tdelta):
            return int(tdelta.seconds / 60)
        
        def _get_seconds(tdelta):
            min = int(tdelta.seconds / 60)
            ret = int(round(((tdelta.seconds / 60) - min) * 60))
            return ret
        '''
        # Get User's Timezone
        user = user_pool.browse(cr, SUPERUSER_ID, uid)
        tz = pytz.timezone(user.partner_id.tz) or False
        if not tz:
            raise osv.except_osv('Error', "Timezone is not defined on this %s user." % user.name)
        
        # Loop Contracts
        for contract in contract_obj.browse(cr, uid, contract_ids, context=context):
            # Variables
            warnings = 0
            val_delay = 0.0 
            val_delay_day = 0.0 
            
            # If work schedule not found skip this contract
            if not contract.working_hours:
                raise osv.except_osv('Error', "Working Schedule is not defined on this %s contract." % contract.name)
                #continue
            
            # Check for delay structure
            if not contract.delay_structure_id:
                raise osv.except_osv('Error', "Delay structure is not defined on this %s contract." % contract.name)
                #continue
            
            # Build rules dictionary
            rules_dic = []
            for rule in contract.delay_structure_id.delay_rule_ids:
                lines = []
                for rule_line in rule.delay_rule_line_ids:
                    lines.append({
                        'rule_line': rule_line.id,
                        'iter': 0,
                    })
                rules_dic.append({
                    'rule': rule.id,
                    'lines': lines,
                })
            
            # Search and Loop attendance records
            
            search_domain = [
                ('name', '>=', date_from),
                ('name', '<=', date_to),
                ('employee_id', '=', contract.employee_id.id),
                ('action','=','sign_in')
            ]
            attendance_ids_first = attendance_obj.search(cr, uid, search_domain, order="name", context=context)
            from_date = datetime.strptime(date_from, "%Y-%m-%d")
            to_date = datetime.strptime(date_to, "%Y-%m-%d")
        
            day = timedelta(days=1)
            all_dates = [(from_date + timedelta(days=x)).date() for x in range((to_date- from_date).days + 1)]
            done  = len(all_dates) * [0]
            
            attendance_ids = []
            for i in range(0,len(all_dates)):
                for attendance in attendance_obj.browse(cr, uid, attendance_ids_first, context=context):
                    
                    att_date = datetime.strptime(attendance.name,"%Y-%m-%d %H:%M:%S").date()
                    
                    if all_dates[i] == att_date and not done[i]:
                        done[i] = 1
                        attendance_ids.append(attendance.id)

               
            
            #attendance_ids = attendance_obj.search(cr, uid, search_domain, order="name", context=context)
            holidays_ids = holidays_obj.search(cr, uid, [('date_from', '>=', date_from),('date_to', '<=', date_to),('employee_id', '=', contract.employee_id.id),('state','=','validate'),('type','=','remove')],context=context)
            test = ''
            attendance_datetime_list = ''
            val_delay_holiday = 0.0
            for holiday in holidays_obj.browse(cr, uid, holidays_ids, context=context):
                leave_from_datetime = pytz.utc.localize(datetime.strptime(holiday.date_from, DATETIME_FORMAT)).astimezone(tz)
                leave_to_datetime = pytz.utc.localize(datetime.strptime(holiday.date_to, DATETIME_FORMAT)).astimezone(tz)
                for attendance1 in attendance_obj.browse(cr, uid, attendance_ids, context=context):  
                    attendance_datetime_x = pytz.utc.localize(datetime.strptime(attendance1.name, DATETIME_FORMAT)).astimezone(tz)
                    if attendance1.action == 'sign_in':
                        if holiday and attendance_datetime_x >= leave_from_datetime and attendance_datetime_x < leave_to_datetime:
                            val_delay = 0
                            attendance_datetime_list += str(attendance_datetime_x) + '\n'
                    elif attendance1.action == 'sign_out':
                        if holiday and attendance_datetime_x >= leave_from_datetime and attendance_datetime_x > leave_to_datetime:
                            val_delay = 0
                            attendance_datetime_list += str(attendance_datetime_x) + '\n'
                #test += attendance_datetime_list + str(leave_from_datetime) + 'holiday' + str(val_delay) + '\n'
            for attendance in attendance_obj.browse(cr, uid, attendance_ids, context=context):  
                attendance_datetime = pytz.utc.localize(datetime.strptime(attendance.name, DATETIME_FORMAT)).astimezone(tz)
                #attendance_date = pytz.utc.localize(datetime.strptime(attendance.name, "%Y-%m-%d")).astimezone(tz)
                #print attendance_datetime.date()
                working_hours_on_day = self.pool.get('resource.calendar').working_hours_on_day(cr, uid, contract.working_hours,datetime.strptime(attendance.name, DATETIME_FORMAT) , context)
                # Check if attendance within working hours
                if not is_in_working_schedule(attendance_datetime, contract.working_hours.id):
                    continue
                
                # Now start checking for delays
                ## Get starting hour of working schedule for this attendance day
                hour_from = get_start_hour_of_the_day(attendance_datetime, contract.working_hours.id)
                hour_from_time = get_time_from_float(hour_from)
                
                ## Inspecting attendance for delays
                for rule in contract.delay_structure_id.delay_rule_ids:
                    # Build rule datetime period
                    start_rule_period = tz.localize(datetime.combine(attendance_datetime.date(), hour_from_time))
                    end_rule_period = start_rule_period + timedelta(minutes=int(rule.delay_period))
                    exempt_period = start_rule_period + timedelta(minutes=int(rule.exempt_minutes))
                    # Calculate difference
                    diff_min_work = attendance_datetime - start_rule_period
                    diff_min_exempt = attendance_datetime - exempt_period
                    # Check within period
                            
                    if str(attendance_datetime) not in attendance_datetime_list and attendance_datetime >= start_rule_period and attendance_datetime < end_rule_period:
                        # Check after exemption

                        if attendance_datetime > exempt_period:
                            # Check if it still a warning
                            if warnings < contract.delay_structure_id.warning_count:
                                
                                warnings += 1
                                break
                            
                            # Apply delays
                            ## Loop delay rule lines
                            calc_done = False
                            for rule_line in rule.delay_rule_line_ids:
                                
                                for rule_dic in rules_dic:
                                    
                                    if rule_dic['rule'] == rule.id and not calc_done:
                                        # Loop rule lines dictionary
                                        for line in rule_dic['lines']:
                                            if line['rule_line'] == rule_line.id:
                                                if line['iter'] < rule_line.iteration:
                                                    line['iter'] += 1
                                                    if rule_line.computation_type == 'fixed':
                                                        val_delay += rule_line.amount
                                                        val_delay_day += rule_line.amount/working_hours_on_day
                                                        calc_done = True
                                                    elif rule_line.computation_type == 'rate':
                                                        if rule.calc_method == 'work':
                                                            val_delay += rule_line.amount * ((diff_min_work.seconds / 60) / 60)
                                                            val_delay_day += rule_line.amount * (((diff_min_work.seconds / 60) / 60)/working_hours_on_day)
                                                            calc_done = True
                                                        elif rule.calc_method == 'exempted':
                                                            val_delay += rule_line.amount * ((diff_min_exempt.seconds / 60) / 60)
                                                            val_delay_day += rule_line.amount * (((diff_min_exempt.seconds / 60) / 60)/working_hours_on_day)

                                                            calc_done = True
                                                elif line['iter'] >= rule_line.iteration and rule_line == rule.delay_rule_line_ids[-1]:
                                                    if rule_line.computation_type == 'fixed':
                                                        val_delay += rule_line.amount
                                                        val_delay_day += rule_line.amount/working_hours_on_day
                                                        calc_done = True
                                                    elif rule_line.computation_type == 'rate':
                                                        if rule.calc_method == 'work':
                                                            val_delay += rule_line.amount * ((diff_min_work.seconds / 60) / 60)
                                                            val_delay_day += rule_line.amount * (((diff_min_work.seconds / 60) / 60)/working_hours_on_day)
                                                            calc_done = True
                                                        elif rule.calc_method == 'exempted':
                                                            val_delay += rule_line.amount * ((diff_min_exempt.seconds / 60) / 60)
                                                            val_delay_day += rule_line.amount * (((diff_min_exempt.seconds / 60) / 60)/working_hours_on_day)
                                                            calc_done = True
                                                
                #test += str(attendance_datetime) + 'Attandance' + str(val_delay) + '\n'
                #print str(attendance_datetime),str(val_delay),str(val_delay_day)
            #raise osv.except_osv(('Vaild'), (test))                                                        
        delays = {
                'name': 'Delays',
                'sequence': 11,
                'code': 'Delays',
                'number_of_days': val_delay_day,
                'number_of_hours': val_delay,
                'contract_id': contract.id,
        }
        res += [delays]
        return res