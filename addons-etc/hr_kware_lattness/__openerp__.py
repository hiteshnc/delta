{
    'name' : "KnowledgeWare HR Lattness",
    'version' : "1.0",
    'author' : "KnowledgeWare Co.",
	'category' : 'HR',
	'website' : 'http://www.kware-eg.com',
    'depends' : ['base','hr','hr_attendance','hr_kware_base_security','hr_contract','hr_kware_payroll'],
	'summary': 'Adding Lattness Management to the HR Module',
    'description':"""
HR Lattness
=====================================
	* Added Lattness Management.
	""",
	'data' : [
		'views/hr_delays_view.xml',
		'views/hr_contract_view.xml',
		'security/ir.model.access.csv',
		'data/hr_delays_data.xml',
	],
}