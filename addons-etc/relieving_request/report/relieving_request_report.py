from openerp import api, models
from openerp.exceptions import ValidationError
class RelievingRequestReport(models.AbstractModel):
    _name = 'report.relieving_request.relieving_request_report_template'

    @api.multi
    def render_html(self, data=None):
        report_obj = self.env['report']
        report = report_obj._get_report_from_name('relieving_request.relieving_request_report_template')
        docargs = {
            'doc_ids': self._ids,
            'doc_model': report.model,
            'docs': self,
            'rel_ids':self.env['relieving.process'].search([('id','in',self.ids)])
	
        }

        return report_obj.render('relieving_request.relieving_request_report_template', docargs)
    
