# -*- coding: utf-8 -*-
{
    'name': 'HR Exit/Relieving Request Process',
    'version': '1.1',
    'category': 'Relieving Request Process',
    'sequence': 1,
    'summary': 'Relieving Request Process',
    'description': """ Relieving Request Process  """,
    'author': 'ADDSOL',
    'website': 'https://www.ktree.com',
    'images': ['images/main-screenshot.png'],
    'depends': ['hr', 'mail'],
    "data":[
            "views/relieving_request_view.xml",
            "views/ir_attachment_view.xml",
            "data/attachment_records.xml",
            "views/relieving_email_template.xml",
            "security/ir.model.access.csv",
            "data/groups.xml",
            "views/relieving_request_template.xml",
            "views/relieving_request_report_view.xml"
            ],
            
    'demo': [],
    'test': [],
    'installable': True,
    'auto_install': False,
    'application': True,
    'price':19.95,
    'currency':'EUR',
    'license':'AGPL-3'
}
