Odoo Relieving Request Process
--------------------
1.This module aims to provide a Relieving Process for the employee.


Relieving Request Process
---------------------------

1. HR Manager will create a Relieving Request for the employee who belongs to their company.

2. Relieving Type can be Resignation, Termination, BGV Fails, Absconding.

3. Based on the Relieving Type tab will be added with the specified fields.

4. After updating all the field values HR Manager will confirm the Relieving Request.

5. Once after the confirmation the email will trigger to Employee, Reporting Manager and Department Manager and the state will be In Progress.

6. In Progress state will have to option Send Clearance Form and Reject .

7. If HR manager rejects the Request the state will move to Rejected.

8. If HR Manager clicks on Send Clearance Form an email will be triggered to employee , admin, Finance manager with attachments added for ExitClearance form and Exit form.

9. After submission of the form HR manager will update the field values in the form and click on Done button and can also reject the request.

10. Reset to Draft button is available in the Rejected State.


Attachment Master
------------------------

1. By Default created the two records Exit Form and Exit Clearance Form here attachment adding option is available so can attach related attachments.

Groups
-----------------------

Created 2 groups under the category Support.
1. Admin Manager. 


Reporting
---------

Can provide Relieving Request Details Report.




