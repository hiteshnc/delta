# -*- coding: utf-8 -*-

from email.header import decode_header
from email.utils import formataddr
import logging

from openerp import _, api, fields, models, SUPERUSER_ID
from openerp import tools
from openerp.osv import expression


class MailTemplate(models.Model):
    
    _inherit = 'mail.template'
    _description = 'Email Templates'
    
    
    #Modifed Email Template for Email from and Email To Address to be updated in mail.mail instead of mail.template object.
    @api.multi
    def send_mail(self, res_id, force_send=False, raise_exception=False):
        """Generates a new mail message for the given template and record,
           and schedules it for delivery through the ``mail`` module's scheduler.

           :param int res_id: id of the record to render the template with
                              (model is taken from the template)
           :param bool force_send: if True, the generated mail.message is
                immediately sent after being created, as if the scheduler
                was executed for this message only.
           :returns: id of the mail.message that was created
        """
        self.ensure_one()
        context = dict(self._context or {})
        Mail = self.env['mail.mail']
        Attachment = self.env['ir.attachment'] 
        # create a mail_mail based on values, without attachments
        values = self.generate_email(res_id)
        values['recipient_ids'] = [(4, pid) for pid in values.get('partner_ids', list())]
        attachment_ids = values.pop('attachment_ids', [])
        attachments = values.pop('attachments', [])
        # add a protection against void email_from
        print values,"*************************888"
        if 'email_from' in values and not values.get('email_from'):
            values.pop('email_from')
        if 'From_relieving' in context:
            values['email_from'] = context.get('email_from',values.get('email_from','')) 
            values['email_to'] = context.get('email_to','')
            values['email_cc'] = context.get('email_cc',values.get('email_cc',''))
            force_send = False   #To Keep the Emails in outgoing state 
        mail = Mail.create(values)

        # manage attachments
        for attachment in attachments:
            attachment_data = {
                'name': attachment[0],
                'datas_fname': attachment[0],
                'datas': attachment[1],
                'res_model': 'mail.message',
                'res_id': mail.mail_message_id.id,
            }
            attachment_ids.append(Attachment.create(attachment_data).id)
        if attachment_ids:
            values['attachment_ids'] = [(6, 0, attachment_ids)]
            mail.write({'attachment_ids': [(6, 0, attachment_ids)]})

        if force_send:
            mail.send(raise_exception=raise_exception)
        return mail.id 
