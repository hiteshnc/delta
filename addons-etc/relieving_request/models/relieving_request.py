# -*- coding: utf-8 -*-
from openerp import api,fields,models
from openerp.tools.translate import _
import pytz
from openerp import SUPERUSER_ID
from datetime import datetime,date
import base64
from openerp.exceptions import ValidationError
from openerp.exceptions import UserError, AccessError

class RelievingProcess(models.Model):
    
    _name = "relieving.process"
    
    _description = "Relieving Process"
    
    TYPE_SELECTION=[
                     ('resignation','Resignation'),
                     ('termination','Termination'),
                     ('bgvfails','BGV Fails'),
                     ('absconding','Absconding'),
                     ]
    
    STATE_SELECTION=[
                     ('new','New'),
                     ('inprogress','In Progress'),
                     ('completed','Completed'),
                     ('rejected','Rejected')
                     ]
         
    @api.model
    def default_get(self,fields):
        '''To set the field create_by, creation_date'''
        
        res = super(RelievingProcess, self).default_get(fields)
        emp_id = self.env['hr.employee'].search([('user_id','=',self.env.uid)])
        if emp_id :
            emp_id = emp_id[0]
        if not emp_id:
            raise ValidationError('Please create Employee for this User !')
        today = datetime.today()
        res['created_by_id'] = emp_id.id
        res['creation_date'] = today.strftime('%Y-%m-%d')
        res['state'] = 'new'
        return res
    
    
    @api.multi
    def get_emails(self,model, view,company_id):
        '''To get group users mail ids'''

        email = ""
        group = self.env['res.groups']
        emp_pool = self.env['hr.employee']
        
        group_id = self.env['ir.model.data'].get_object_reference(model, view)[1]
        group_obj = group.browse(group_id)
        users = [user for user in group_obj.users]
        for user in users:
            emp_obj = emp_pool.search([('user_id','=',user.id)])
            if emp_obj and emp_obj[0].company_id.id == company_id:
                mail_id = str(emp_obj[0].work_email)
                if mail_id:
                    email += str(mail_id) + ","
        return email
    
   

    name = fields.Many2one('hr.employee','Employee')
    department_id = fields.Many2one('hr.department','Department')
    relieving_type = fields.Selection(TYPE_SELECTION,'Relieving Type')
    reporting_manager_id = fields.Many2one('hr.employee','Reporting Manager')
    state = fields.Selection(STATE_SELECTION,'STATE')
    relieving_date = fields.Date('Relieving Date')
    created_by_id = fields.Many2one('hr.employee','Relieving Request Created By')
    creation_date = fields.Date('Relieving Created Date')
    company_id = fields.Many2one('res.company','company')
    final_amt = fields.Float('Full & Final Amount')
    add_sub = fields.Float('Addition/ Deduction')
    total = fields.Float('Total')
    Note = fields.Text('Note')
    
    @api.onchange('final_amt', 'add_sub')
    def onchange_full_final(self):
        if self.final_amt or self.add_sub:
            self.total = self.final_amt + self.add_sub
    
    #fields under Resignation Tab
    
    resignation_inform_rm_hr = fields.Boolean('Informed RM and HR')
    resignation_abideby_exit_policy = fields.Boolean('Exit Policy Verification')
    resignation_notice_period_approved = fields.Boolean('Notice Period Approved')
    resignation_exit_clearance_form_provided = fields.Boolean('Exit Clearance Form Provided')
    resignation_admin_clearance = fields.Boolean('Admin Clearance')
    resignation_finanace_clearance = fields.Boolean('Finance Clearance')
    resignation_it_clearance = fields.Boolean('IT Clearance')
    resignation_exit_clearance_form_received = fields.Boolean('Exit Clearance Form Received')
    resignation_provide_relieving_document = fields.Boolean('Provide Relieving Document')
    resignation_email_received_for_admin_clearance = fields.Boolean('Email Recieved')
    resignation_email_received_for_finance_clearance = fields.Boolean('Email Recieved')
    resignation_email_received_for_it_clearance = fields.Boolean('Email Recieved')
    
    #fields under Termination Tab
    
    termination_manager_hr_approval = fields.Boolean('Manager and HR approval to process request')
    termination_exit_clearance_form_provided = fields.Boolean('Exit Clearance Form Provided')
    termination_admin_clearance=fields.Boolean('Admin Clearance')
    termination_finanace_clearance=fields.Boolean('Finance Clearance')
    termination_it_clearance=fields.Boolean('IT Clearance')
    termination_exit_clearance_form_received=fields.Boolean('Exit Clearance Form Received')
    termination_provide_relieving_document=fields.Boolean('Provide Relieving Document')
    termination_email_received_for_admin_clearance=fields.Boolean('Email Recieved')
    termination_email_received_for_finance_clearance=fields.Boolean('Email Recieved')
    termination_email_received_for_it_clearance=fields.Boolean('Email Recieved')
    
    #fields under bgvfails Tab
    
    bgvfails_manager_hr_approval=fields.Boolean('Manager and HR approval to process request')
    bgvfails_exit_clearance_form_provided = fields.Boolean('Exit Clearance Form Provided')
    bgvfails_admin_clearance = fields.Boolean('Admin Clearance')
    bgvfails_finanace_clearance = fields.Boolean('Finance Clearance')
    bgvfails_it_clearance = fields.Boolean('IT Clearance')
    bgvfails_exit_clearance_form_received = fields.Boolean('Exit Clearance Form Received')
    bgvfails_provide_relieving_document = fields.Boolean('Provide Relieving Document')
    bgvfails_email_received_for_admin_clearance = fields.Boolean('Email Recieved')
    bgvfails_email_received_for_finance_clearance = fields.Boolean('Email Recieved')
    bgvfails_email_received_for_it_clearance = fields.Boolean('Email Recieved')
    
    #Field under Absconding Tab
    
    absconding_manager_hr_approval = fields.Boolean('Manager and HR approval to process request')
    absconding_exit_clearance_form_provided = fields.Boolean('Exit Clearance Form Provided')
    absconding_admin_clearance = fields.Boolean('Admin Clearance')
    absconding_finanace_clearance = fields.Boolean('Finance Clearance')
    absconding_it_clearance = fields.Boolean('IT Clearance')
    absconding_exit_clearance_form_received = fields.Boolean('Exit Clearance Form Received')
    absconding_provide_relieving_document  = fields.Boolean('Provide Relieving Document')
    absconding_email_received_for_admin_clearance = fields.Boolean('Email Recieved')
    absconding_email_received_for_finance_clearance = fields.Boolean('Email Recieved')
    absconding_email_received_for_it_clearance = fields.Boolean('Email Recieved')
    file_name = fields.Binary('Filename')
    clearance_form_sent = fields.Boolean('Clearance Form Received')
   
    last_updated_by = fields.Many2one('res.users','Last Updated by')
   
    @api.onchange('name')
    def onchange_employee(self):
        ''' to set the field department_id based on the selection of employee'''
        print 'name---', self.name
        if self.name:
            emp_obj = self.env['hr.employee'].search([('id','=',self.name.id)])
            print 'emp_obj', emp_obj
            if emp_obj :
                emp_obj = emp_obj[0]
                self.department_id = emp_obj.department_id.id 
                self.reporting_manager_id = emp_obj.parent_id.id
                self.company_id = emp_obj.company_id.id
                print 'company---', self.company_id
                
        
   
    @api.model
    def search(self, args, offset=0, limit=None, order=None, count=False):

        users_obj = self.env["res.users"]

        user_company = users_obj.browse(self.env.uid).company_id.id

        is_hr = users_obj.has_group('base.group_hr_manager')

        if self.env.uid != 1 and is_hr:
            
            args += [('company_id','=',user_company)]

            return super(RelievingProcess, self).search(args, offset=0, limit=None, order=None, count=False)

        else :
            return super(RelievingProcess, self).search(args, offset=0, limit=None, order=None, count=False)

    @api.model
    def read_group(self, domain, fields, groupby, offset=0, limit=None, orderby=False,lazy=True):

        users_obj = self.env["res.users"]

        user_company = users_obj.browse(self.env.uid).company_id.id

        is_hr = users_obj.has_group('base.group_hr_manager')

        if self.env.uid != 1 and is_hr:
            
            domain += [('company_id','=',user_company)]

            return super(RelievingProcess, self).read_group(domain, fields, groupby, offset=0, limit=None, orderby=False,lazy=True)

        else :
            return super(RelievingProcess, self).read_group(domain, fields, groupby, offset=0, limit=None, orderby=False,lazy=True)
 
    @api.one
    def button_confirm(self):
        
        ''' To change the state from new to in progress and checks the necessary conditions  to change the state'''

        obj = self

        if not obj.reporting_manager_id:
            
            raise ValidationError('Reporting manager is not set for the record')
        
        
        if obj.relieving_type == 'resignation': 
        
            if obj.resignation_inform_rm_hr and obj.resignation_abideby_exit_policy and obj.resignation_notice_period_approved:               
                self.write({'state':'inprogress'})
        
            else:
                raise ValidationError('For further processing Need to have Notice period approval as TRUE !')
        
        if obj.relieving_type =='bgvfails':
            
            if obj.bgvfails_manager_hr_approval:
                self.write({'state':'inprogress'})
            else:
                raise ValidationError('For further processing Need to have Manager and HR Approval as TRUE !')
        
        
        if obj.relieving_type =='termination':
            
            if obj.termination_manager_hr_approval:
                self.write({'state':'inprogress'})
            else:
                raise ValidationError('For further processing Need to have Manager and HR Approval as TRUE !')
            
        if obj.relieving_type =='absconding':
            
            if obj.absconding_manager_hr_approval:
                self.write({'state':'inprogress'})
            else:
                raise ValidationError('For further processing Need to have Manager and HR Approval as TRUE !')
           
        ir_model_data = self.env['ir.model.data']
        try:
            template_id = ir_model_data.get_object_reference('relieving_request', 'email_template_email_for_relieving_to_confirm')[1]
        except ValueError:
            template_id = False
        if template_id :
            template_pool = self.env['mail.template']
            
            emp_obj = self.env['hr.employee'].search([('user_id', '=', self.env.uid)])
            if emp_obj and emp_obj[0].work_email:
                email_from = str(emp_obj[0].work_email)
            email_cc = self.get_emails('base','group_hr_manager',self.company_id.id) 
            email_cc = email_cc + (self.reporting_manager_id.work_email or '') 
            email_to = self.name.work_email or ''
            if email_from and email_to :
                template_pool.browse(template_id).with_context({'From_relieving':True,'email_cc':email_cc,'email_to':email_to,'email_from':email_from}).send_mail(self.id,force_send=True)
        return True

    
    @api.one
    def send_clearance_form(self):
        '''To send the email with the attachment to the employee tagged'''
        
        relieving_obj = self
        company_id = relieving_obj.company_id.id
        email_to = ''   
        if relieving_obj.name:
            if relieving_obj.name.work_email:
                email_to = str(relieving_obj.name.work_email)
            
        user_obj = self.env['res.users'].browse(self.env.uid)
        
        email_from = user_obj.email and user_obj.email or False
        
        email_cc = ''

        if relieving_obj.reporting_manager_id.user_id:
            if relieving_obj.reporting_manager_id.user_id.email:
                email_cc = str(relieving_obj.reporting_manager_id.user_id.email)+","

        '''To get the email of user of group - Manager'''
        email_cc = self.get_emails('base','group_hr_manager',self.company_id.id)
        
        '''To get the email of user of group - Finanace Manager'''
        email_cc = email_cc + self.get_emails('account','group_account_manager',self.company_id.id)        
        
        '''To get the email of users of group - Admin Manager'''
        email_cc = email_cc + self.get_emails('relieving_request','group_admin_managers',self.company_id.id)
        
        
        '''To get the email of users of group IT Manager '''
        email_cc = email_cc + self.get_emails('relieving_request','group_it_managers',self.company_id.id)
        
        
        '''To get the email of related department manager'''
        
        if self.name.department_id and self.name.department_id.manager_id and self.name.department_id.manager_id.work_email :
            email_cc = email_cc + self.name.department_id.manager_id.work_email
        
        '''To get the email template for Relieving Send Exit Interview'''
        template_pool = self.env['mail.template']
        if relieving_obj.relieving_type == 'resignation':
            try:
                template_id = self.env['ir.model.data'].get_object_reference('relieving_request', 'email_template_email_for_relieving_tosend_exitinterview')[1]
            except ValueError:
                template_id = False
            if template_id :
                if email_from and email_to :
                    template_pool.browse(template_id).with_context({'From_relieving':True,'email_cc':email_cc,'email_to':email_to,'email_from':email_from}).send_mail(self.id,force_send=True)

        try:
            template_id = self.env['ir.model.data'].get_object_reference('relieving_request', 'email_template_email_for_relieving_tosend_exitclearance')[1]
        except ValueError:
            template_id = False
        if template_id :
            if email_from and email_to :
                template_pool.browse(template_id).with_context({'From_relieving':True,'email_cc':email_cc,'email_to':email_to,'email_from':email_from}).send_mail(self.id,force_send=True)
    
        if relieving_obj.relieving_type == 'resignation':
            self.write({'resignation_exit_clearance_form_provided':True})
        
        if relieving_obj.relieving_type == 'termination':
            self.write({'termination_exit_clearance_form_provided':True})
        
        if relieving_obj.relieving_type == 'absconding':
            self.write({'absconding_exit_clearance_form_provided':True})
        
        if relieving_obj.relieving_type == 'bgvfails':
            self.write({'bgvfails_exit_clearance_form_provided':True})
        self.write({'clearance_form_sent':True})
        return True


 
 
    @api.one
    def button_completed(self):
        
        '''changes the state form In Progress to Completed and checks the necessary conditions.
        Updates the relieving_date field'''
        
        today = datetime.today()
        today = today.strftime('%Y-%m-%d')
        obj = self
         
        if obj.relieving_type == 'resignation':
             if obj.resignation_exit_clearance_form_provided and obj.resignation_admin_clearance and obj.resignation_finanace_clearance and obj.resignation_it_clearance and obj.resignation_exit_clearance_form_received and obj.resignation_provide_relieving_document:
                 self.write({'state':'completed','relieving_date':today})
             else :
                  raise ValidationError('Can not update the state because all the clearances must be done !')
              
        if obj.relieving_type == 'termination':
             
             if obj.termination_exit_clearance_form_provided and obj.termination_admin_clearance and obj.termination_finanace_clearance and obj.termination_it_clearance and obj.termination_exit_clearance_form_received and obj.termination_provide_relieving_document:
                 self.write({'state':'completed','relieving_date':today})
             else :
                  raise ValidationError('Can not update the state because all the clearances must be done !')
                 
        if obj.relieving_type == 'bgvfails':
             
             if obj.bgvfails_exit_clearance_form_provided and obj.bgvfails_admin_clearance and obj.bgvfails_finanace_clearance and obj.bgvfails_it_clearance and obj.bgvfails_exit_clearance_form_received and obj.bgvfails_provide_relieving_document:
                 self.write({'state':'completed','relieving_date':today})
             else :
                  raise ValidationError('Can not update the state because all the clearances must be done !')

        if obj.relieving_type == 'absconding':
             
             if obj.absconding_exit_clearance_form_provided and obj.absconding_admin_clearance and obj.absconding_finanace_clearance and obj.absconding_it_clearance and obj.absconding_exit_clearance_form_received and obj.absconding_provide_relieving_document:
                 self.write({'state':'completed','relieving_date':today})
             else :
                  raise ValidationError('Can not update the state because all the clearances must be done !')
        
        ''' To send the mail to the Department Manager and Employee Manager if the state is Done'''

        try:
            template_id = self.env['ir.model.data'].get_object_reference('relieving_request', 'email_template_email_for_relieving_complete')[1]
        except ValueError:
            template_id = False
        if template_id :
            emp_obj = self.env['hr.employee']
            emp_id = emp_obj.search([('user_id','=',self.env.uid)])
            email_from = ''
            if emp_id :
                email_from = emp_id[0].work_email
            email_to = self.name.work_email or ''
            if email_to :
                email_to = email_to + ','
            if  self.reporting_manager_id and self.reporting_manager_id.work_email :
                email_to = email_to + self.reporting_manager_id.work_email
            template_pool = self.env['mail.template']
            if email_from and email_to :
                template_pool.browse(template_id).with_context({'From_relieving':True,'email_to':email_to,'email_from':email_from}).send_mail(self.id,force_send=True)
        return True    

    @api.one 
    def button_rejected(self):
        '''changes the state from In Progress to Rejected'''

        try:
            template_id = self.env['ir.model.data'].get_object_reference('relieving_request', 'email_template_email_for_relieving_to_rejected')[1]
        except ValueError:
            template_id = False
        if template_id :
            emp_obj = self.env['hr.employee']
            emp_id = emp_obj.search([('user_id','=',self.env.uid)])
            email_from = ''
            if emp_id :
                email_from = emp_id[0].work_email
            email_to = self.name.work_email or ''
            if email_to :
                email_to = email_to + ','
            if  self.reporting_manager_id and self.reporting_manager_id.work_email :
                email_cc = self.reporting_manager_id.work_email
            template_pool = self.env['mail.template']
            if email_from and email_to :
                template_pool.browse(template_id).with_context({'From_relieving':True,'email_cc':email_cc,'email_to':email_to,'email_from':email_from}).send_mail(self.id,force_send=True)
        return self.write({'state':'rejected','clearance_form_sent':False})
    
    @api.one
    def button_reset_draft(self):
        '''Changes the state from Rejected to new'''
        
        obj = self
        
        if obj.relieving_type == 'resignation': 
            
            self.write({'resignation_inform_rm_hr':False,'resignation_abideby_exit_policy':False,'resignation_notice_period_approved':False,'resignation_exit_clearance_form_provided':False,'resignation_admin_clearance':False,'resignation_finanace_clearance':False,'resignation_it_clearance':False,'resignation_exit_clearance_form_received':False,'resignation_email_received_for_admin_clearance':False,'resignation_email_received_for_finance_clearance':False,'resignation_email_received_for_it_clearance':False})
            
        if obj.relieving_type == 'termination':
            
            self.write({'termination_manager_hr_approval':False,'termination_exit_clearance_form_provided':False,'termination_admin_clearance':False,'termination_finanace_clearance':False,'termination_it_clearance':False,'termination_exit_clearance_form_received':False,'termination_email_received_for_admin_clearance':False,'termination_email_received_for_finance_clearance':False,'termination_email_received_for_it_clearance':False})

        if obj.relieving_type == 'bgvfails':
            
            self.write({'bgvfails_manager_hr_approval':False,'bgvfails_exit_clearance_form_provided':False,'bgvfails_admin_clearance':False,'bgvfails_finanace_clearance':False,'bgvfails_it_clearance':False,'bgvfails_exit_clearance_form_received':False,'bgvfails_email_received_for_admin_clearance':False,'bgvfails_email_received_for_finance_clearance':False,'bgvfails_email_received_for_it_clearance':False})

        if obj.relieving_type == 'absconding':
            
            self.write({'absconding_manager_hr_approval':False,'absconding_exit_clearance_form_provided':False,'absconding_admin_clearance':False,'absconding_finanace_clearance':False,'absconding_it_clearance':False,'absconding_exit_clearance_form_received':False,'absconding_email_received_for_admin_clearance':False,'absconding_email_received_for_finance_clearance':False,'absconding_email_received_for_it_clearance':False})

        try:
            template_id = self.env['ir.model.data'].get_object_reference('relieving_request', 'email_template_email_for_relieving_to_reset')[1]
        except ValueError:
            template_id = False
        if template_id :
            emp_obj = self.env['hr.employee']
            emp_id = emp_obj.search([('user_id','=',self.env.uid)])
            email_from = ''
            if emp_id :
                email_from = emp_id[0].work_email
            email_to = self.name.work_email or ''
            if email_to :
                email_to = email_to + ','
            email_cc = ''
            if self.department_id and self.department_id.manager_id and self.department_id.manager_id.work_email :
                email_cc = self.department_id and self.department_id.manager_id and self.department_id.manager_id.work_email + ','
            if  self.reporting_manager_id and self.reporting_manager_id.work_email :
                email_cc = email_cc + self.reporting_manager_id.work_email
            template_pool = self.env['mail.template']
            if email_from and email_to :
                template_pool.browse(template_id).with_context({'From_relieving':True,'email_cc':email_cc,'email_to':email_to,'email_from':email_from}).send_mail(self.id,force_send=True)
        self.write({'state':'new'})
        
        return True
    
    
    
    @api.multi    
    def unlink(self):
     
        '''To restrict the user to delete the record other than new state'''
             
        for rec in self:
            if rec.state<>'new':
                raise ValidationError('You cannot delete a Relieving Request which is not in new state !')
        return super(RelievingProcess, self).unlink()
    
    @api.onchange('resignation_inform_rm_hr')    
    def onchange_resignation_inform_rm_hr(self):
                
        if self.resignation_inform_rm_hr == False:
            self.resignation_abideby_exit_policy = False
        
    
    @api.onchange('resignation_abideby_exit_policy')
    def onchange_resignation_abideby_exit_policy(self):

        if self.resignation_abideby_exit_policy == False:
            self.resignation_notice_period_approved = False
        
    
    @api.onchange('resignation_notice_period_approved')
    def onchange_resignation_notice_period_approved(self):

        if self.resignation_notice_period_approved==False:
            self.resignation_exit_clearance_form_provided = False
    
    @api.onchange('resignation_exit_clearance_form_provided')
    def onchange_resignation_exit_clearance_form_provided(self):     

        if self.resignation_exit_clearance_form_provided==False:
            self.resignation_admin_clearance = False
            self.resignation_email_received_for_admin_clearance = False
            self.resignation_finanace_clearance = False
            self.resignation_email_received_for_finance_clearance = False
            self.resignation_it_clearance = False
            self.resignation_email_received_for_it_clearance= False
   
    @api.onchange('resignation_exit_clearance_form_received')    
    def onchange_resignation_exit_clearance_form_received(self):
        
        if self.resignation_exit_clearance_form_received==False:
            self.resignation_provide_relieving_document = False
        
    
    @api.onchange('termination_admin_clearance','resignation_finanace_clearance','resignation_admin_clearance')
    def onchange_clearances(self):
        
        if self.resignation_it_clearance==False or self.resignation_finanace_clearance==False or self.resignation_admin_clearance==False:
            self.resignation_exit_clearance_form_received = False
        
        
        
        
    '''onchange function for the fields under Termination
    Onchange functions to make the boolean Field False when they are invisible'''
    
    @api.onchange('termination_manager_hr_approval')
    def onchange_termination_manager_hr_approval(self):
        
        if self.termination_manager_hr_approval==False:
            self.termination_exit_clearance_form_provided = False
    
    @api.onchange('termination_exit_clearance_form_provided')
    def onchange_termination_exit_clearance_form_provided(self):
       
        if self.termination_exit_clearance_form_provided==False:
            termination_provide_relieving_document = False

    
    @api.onchange('termination_exit_clearance_form_received')
    def onchange_termination_exit_clearance_form_received(self):
        
        if self.termination_exit_clearance_form_received==False:
            self.termination_provide_relieving_document = False
    
    @api.onchange('termination_finanace_clearance','termination_admin_clearance','termination_it_clearance')
    def onchange_clearances_termination(self):
        
        if self.termination_admin_clearance == False or self.termination_finanace_clearance == False or self.termination_it_clearance==False:
            self.termination_exit_clearance_form_received = False
            

    '''onchange functions for the fields under Absconding
    Onchange functions to make the boolean Field False when they are invisible'''
    
    @api.onchange('absconding_manager_hr_approval')
    def onchange_absconding_manager_hr_approval(self):
        
        if self.absconding_manager_hr_approval==False:
            self.absconding_exit_clearance_form_provided = False
    
    @api.onchange('absconding_exit_clearance_form_provided')
    def onchange_absconding_exit_clearance_form_provided(self):

        if self.absconding_exit_clearance_form_provided==False:
            self.absconding_admin_clearance = False
    
    @api.onchange('absconding_exit_clearance_form_received')
    def onchange_absconding_exit_clearance_form_received(self):
        
        if self.absconding_exit_clearance_form_received==False:
            self.absconding_provide_relieving_document = False
    
    @api.onchange('absconding_it_clearance','absconding_admin_clearance','absconding_finanace_clearance')
    def onchange_clearances_absconding(self):
    
        if self.absconding_admin_clearance == False or self.absconding_finanace_clearance == False or self.absconding_it_clearance == False:
            self.absconding_exit_clearance_form_received = False
    
    
    '''onchange functions for the fields under bgvfails
    Onchange functions to make the boolean Field False when they are invisible'''
   
    @api.onchange('bgvfails_manager_hr_approval')
    def onchange_bgvfails_manager_hr_approval(self):

        if self.bgvfails_manager_hr_approval==False:
            self.bgvfails_exit_clearance_form_provided = False
  
    @api.onchange('bgvfails_exit_clearance_form_provided')
    def onchange_bgvfails_exit_clearance_form_provided(self):

        if self.bgvfails_exit_clearance_form_provided == False:
            self.bgvfails_admin_clearance = False
            
    @api.onchange('bgvfails_exit_clearance_form_received')
    def onchange_bgvfails_exit_clearance_form_received(self):
        
        if self.bgvfails_exit_clearance_form_received == False:
            self.bgvfails_provide_relieving_document = False
    
    @api.onchange('bgvfails_admin_clearance','bgvfails_finanace_clearance','bgvfails_it_clearance')
    def onchange_clearance_bgvfails(self):
        
        if self.bgvfails_admin_clearance == False or self.bgvfails_finanace_clearance==False or self.bgvfails_it_clearance==False:    
            self.bgvfails_exit_clearance_form_received = False
    
    @api.model
    def create(self,vals):

        user_company_id = self.env["res.users"].browse(self.env.uid).company_id.id
        print 'user_company_id----', user_company_id, 'company----', user_company_id
        cur_company_id = user_company_id
        print 'current_company_id-----', cur_company_id
        vals['company_id'] = cur_company_id
        if cur_company_id != user_company_id :

            raise ValidationError("You can not create the other Company's Associate's Relieving Request")
        
        '''if the selection type is resignation then all other boolean fields which are under other tabs will set to False'''
        if vals.has_key('relieving_type'):
            if vals['relieving_type'] == 'resignation':
                vals['termination_manager_hr_approval'] = False
                vals['bgvfails_manager_hr_approval']=False
                vals['absconding_manager_hr_approval']=False
        
        '''if the selection type is termination then all other boolean fields which are under other tabs will set to False'''
        if vals.has_key('relieving_type'):
            if vals['relieving_type'] == 'termination':    
                vals['resignation_inform_rm_hr']=False
                vals['resignation_abideby_exit_policy']=False
                vals['resignation_notice_period_approved']=False
                vals['bgvfails_manager_hr_approval']=False
                vals['absconding_manager_hr_approval']=False
        
        '''if the selection type is bgvfails then all other boolean fields which are under other tabs will set to False'''
        if vals.has_key('relieving_type'):
            if vals['relieving_type'] == 'bgvfails':
                vals['resignation_inform_rm_hr']=False
                vals['resignation_abideby_exit_policy']=False
                vals['resignation_notice_period_approved']=False
                vals['termination_manager_hr_approval']=False
                vals['absconding_manager_hr_approval']=False
        
        '''if the selection type is absconding then all other boolean fields which are under other tabs will set to False'''
        if vals.has_key('relieving_type'):

            if vals['relieving_type'] == 'absconding':
                
                vals['resignation_inform_rm_hr']=False
                vals['resignation_abideby_exit_policy']=False
                vals['resignation_notice_period_approved']=False
                vals['termination_manager_hr_approval']=False
                vals['bgvfails_manager_hr_approval']=False
        print 'vals----', vals   
        return super(RelievingProcess,self).create(vals)

    @api.multi
    def write(self, vals):

        user_company_id = self.env["res.users"].browse(self.env.uid).company_id.id

        cur_company_id = self.company_id.id

        if cur_company_id != user_company_id :

            raise ValidationError("You can not modify the other company's Relieving Request")
        
        '''if the selection type is resignation then all other boolean fields which are under other tabs will set to False'''
        if vals.has_key('relieving_type'):
            if vals['relieving_type'] == 'resignation':
                vals['termination_manager_hr_approval'] = False
                vals['bgvfails_manager_hr_approval']=False
                vals['absconding_manager_hr_approval']=False
        
        '''if the selection type is termination then all other boolean fields which are under other tabs will set to False'''
        if vals.has_key('relieving_type'):
            if vals['relieving_type'] == 'termination':    
                vals['resignation_inform_rm_hr']=False
                vals['resignation_abideby_exit_policy']=False
                vals['resignation_notice_period_approved']=False
                vals['bgvfails_manager_hr_approval']=False
                vals['absconding_manager_hr_approval']=False
        
        '''if the selection type is bgvfails then all other boolean fields which are under other tabs will set to False'''
        if vals.has_key('relieving_type'):
            if vals['relieving_type'] == 'bgvfails':
                vals['resignation_inform_rm_hr']=False
                vals['resignation_abideby_exit_policy']=False
                vals['resignation_notice_period_approved']=False
                vals['termination_manager_hr_approval']=False
                vals['absconding_manager_hr_approval']=False
        
        '''if the selection type is absconding then all other boolean fields which are under other tabs will set to False'''
        if vals.has_key('relieving_type'):

            if vals['relieving_type'] == 'absconding':
                
                vals['resignation_inform_rm_hr']=False
                vals['resignation_abideby_exit_policy']=False
                vals['resignation_notice_period_approved']=False
                vals['termination_manager_hr_approval']=False
                vals['bgvfails_manager_hr_approval']=False
        
        vals['last_updated_by'] = self.env.uid

        return super(RelievingProcess,self).write(vals)
            
 

RelievingProcess()

class ir_attachment(models.Model):
    
    _inherit = "ir.attachment"
    
    from_relieving = fields.Boolean('From Relieving')
    
    def unlink(self, cr, uid, ids, context=None):
        for rec in self.browse(cr, uid, ids, context=context):
            if rec.from_relieving:
                raise UserError(_('You cannot delete records'))
        return super(ir_attachment, self).unlink(cr, uid, ids, context)
    
    
ir_attachment()
