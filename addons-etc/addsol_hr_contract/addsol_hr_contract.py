# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2015-2016 Addition IT Solutions Pvt. Ltd. (<http://www.aitspl.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import tools, api
from openerp import models, fields, api, _



class addsol_hr_contract(models.Model):
	_inherit = 'hr.contract'
	
	da = fields.Float("DA")
	ta = fields.Float("TA")
	hq_travel = fields.Float("EX HQ")
	non_hq_travel = fields.Float("Non HQ")
	mobile_expenses = fields.Float("Mobile Expenses")
	stationary_expesnes = fields.Float("Stationary Expenses")
	expense_limit = fields.Float("Expenses Limit")
	pf = fields.Boolean("Provident Fund")
	esic = fields.Boolean("ESIC")
	mah_uk = fields.Boolean("Is Maharashtra?")
	sales = fields.Boolean("Sales")	
