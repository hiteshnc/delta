#import time
import pytz
from openerp import SUPERUSER_ID
from datetime import datetime,timedelta,time
from dateutil.relativedelta import relativedelta
from openerp import netsvc
from openerp.osv import fields, osv
from openerp import tools
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp

from openerp.tools.safe_eval import safe_eval as eval

class hr_payslip_kware_att(osv.Model):
    _inherit = 'hr.payslip'
    
    
    def get_worked_day_lines(self, cr, uid, contract_ids, date_from, date_to, context=None):
        """
        @param contract_ids: list of contract id
        @return: returns a list of dict containing the input that should be applied for the given contract between date_from and date_to
        """
        res = super(hr_payslip_kware_att,self).get_worked_day_lines(cr,uid,contract_ids,date_from,date_to,context=context)
        #print "In"
        def was_on_leave(employee_id, datetime_day, context=None):
            res = False
            day = datetime_day.strftime("%Y-%m-%d")
            holiday_ids = self.pool.get('hr.holidays').search(cr, uid, [('state','=','validate'),('employee_id','=',employee_id),('type','=','remove'),('date_from','<=',day),('date_to','>=',day)])
            if holiday_ids:
                res = holiday_ids
            return res

        
        attendance_obj = self.pool.get('hr.attendance')
        for contract in self.pool.get('hr.contract').browse(cr, uid, contract_ids, context=context):
           
            if not contract.working_hours:
                raise osv.except_osv('Error', "Working Schedule is not defined on this %s contract." % contract.name)

            
            # Check for delay structure
            if not contract.delay_structure_id:
                raise osv.except_osv('Error', "Delay structure is not defined on this %s contract." % contract.name)

            ab_rate = contract.delay_structure_id.absence_rule_rate
            #no_sign_in_rule = contract.delay_structure_id.no_sign_in_rule
            no_sign_in_rule_rate = contract.delay_structure_id.no_sign_in_rule_rate
            #no_sign_out_rule = contract.delay_structure_id.no_sign_out_rule
            no_sign_out_rule_rate = contract.delay_structure_id.no_sign_out_rule_rate
            attendances = {
                 'name': _("Actual Attendance Days"),
                 'sequence': 1,
                 'code': 'WORK100',
                 'number_of_days': 0.0,
                 'number_of_hours': 0.0,
                 'contract_id': contract.id,
            }
            absence_days = {
                 'name': _("Absent Days"),
                 'sequence': 2,
                 'code': 'Absent',
                 'number_of_days': 0.0,
                 'number_of_hours': 0.0,
                 'contract_id': contract.id,
            }
            no_sign_in_line = {
                 'name': _("No Sign In Days"),
                 'sequence': 3,
                 'code': 'no_sign_in',
                 'number_of_days': 0.0,
                 'number_of_hours': 0.0,
                 'contract_id': contract.id,
            }
            no_sign_out_line = {
                 'name': _("No Sign Out Days"),
                 'sequence': 4,
                 'code': 'no_sign_out',
                 'number_of_days': 0.0,
                 'number_of_hours': 0.0,
                 'contract_id': contract.id,
            }
            day_from = datetime.strptime(date_from,"%Y-%m-%d")
            day_to = datetime.strptime(date_to,"%Y-%m-%d")
            nb_of_days = (day_to - day_from).days +1
            for day in range(0, nb_of_days):
                no_sign_in = False
                no_sign_out = False
                working_hours_on_day = self.pool.get('resource.calendar').working_hours_on_day(cr, uid, contract.working_hours, day_from + timedelta(days=day), context)
                if working_hours_on_day:
                    hl_ids =  was_on_leave(contract.employee_id.id,(day_from + timedelta(days=day)))
                    leave_on_day = False
                    if hl_ids:
                        for leave in self.pool.get('hr.holidays').browse(cr, uid,hl_ids,context):
                            #if he was on leave, fill the leaves dict
                            daytime_from_leave = datetime.strptime(leave.date_from,"%Y-%m-%d %H:%M:%S")
                            daytime_to_leave = datetime.strptime(leave.date_to,"%Y-%m-%d %H:%M:%S")
                            hours_difference = ((daytime_to_leave-daytime_from_leave).days * 24) + ((daytime_to_leave-daytime_from_leave).seconds / 60.0 / 60.0)
                            if hours_difference >= working_hours_on_day:
                                leave_on_day = True
                                break
                    if leave_on_day :
                        continue

                    start_datetime = datetime.combine((day_from + timedelta(days=day)), time(0, 0))
                    end_datetime = datetime.combine((day_from + timedelta(days=day)), time(23, 59))
                    start_datetime_str = datetime.strftime(start_datetime,"%Y-%m-%d %H:%M:%S")
                    end_datetime_str = datetime.strftime(end_datetime,"%Y-%m-%d %H:%M:%S")

                     #print 'date form',date_from
                    search_domain_in = [
                        ('name', '>=', start_datetime_str),
                        ('name', '<=', end_datetime_str),
                        ('employee_id', '=', contract.employee_id.id),
                        ('action','=','sign_in'),
                    ]
                    attendance_in_ids = attendance_obj.search(cr, uid, search_domain_in, order="name", context=context)
                    if not attendance_in_ids:
                        no_sign_in = True
                        

                    search_domain_out = [
                        ('name', '>=', start_datetime_str),
                        ('name', '<=', end_datetime_str),
                        ('employee_id', '=', contract.employee_id.id),
                        ('action','=','sign_out'),
                    ]

                    attendance_out_ids = attendance_obj.search(cr, uid, search_domain_out, order="name", context=context)
                    if not attendance_out_ids:
                        no_sign_out = True
                        

                    if no_sign_in == True and no_sign_out == False:
                        no_sign_in_line['number_of_days'] += 1.0 * no_sign_in_rule_rate
                        no_sign_in_line['number_of_hours'] += working_hours_on_day * no_sign_in_rule_rate
                        continue
                    if no_sign_in == False and no_sign_out == True:
                        no_sign_out_line['number_of_days'] += 1.0 * no_sign_out_rule_rate
                        no_sign_out_line['number_of_hours'] += working_hours_on_day * no_sign_out_rule_rate
                        continue
                    if no_sign_in == True and no_sign_out == True:
                        absence_days['number_of_days'] += 1.0 * ab_rate
                        absence_days['number_of_hours'] += working_hours_on_day * ab_rate
                        continue

                    in_out_pairs = []
                    if len(attendance_in_ids) == 1:
                        att_in = attendance_obj.browse(cr,uid,attendance_in_ids,context=context)[0]
                        att_out = attendance_obj.browse(cr,uid,attendance_out_ids,context=context)[0]
                        in_out_pairs.append((datetime.strptime(att_in.name,"%Y-%m-%d %H:%M:%S"),datetime.strptime(att_out.name,"%Y-%m-%d %H:%M:%S")))

                    else:


                        sign_in_datetime =[]
                        sign_out_datetime =[]
                        sign_in_note = []
                        sign_out_note = []
                        for att_in in attendance_obj.browse(cr,uid,attendance_in_ids,context=context):
                            sign_in_datetime.append(datetime.strptime(att_in.name,"%Y-%m-%d %H:%M:%S"))
                        sign_in_note  = len(sign_in_datetime) * ['in']
                        for att_out in attendance_obj.browse(cr,uid,attendance_out_ids,context=context):
                            sign_out_datetime.append(datetime.strptime(att_out.name,"%Y-%m-%d %H:%M:%S"))
                        sign_out_note  = len(sign_out_datetime) * ['out']

                        attend_list = sign_in_datetime + sign_out_datetime
                        attend_type = sign_in_note+sign_out_note
                        attend_list_sorted , attend_type_sorted = (list(t) for t in zip(*sorted(zip(attend_list, attend_type))))
                        #print attend_list_sorted ,attend_type_sorted
                        index = 0
                        while (len(attend_type_sorted) > 1):
                            if attend_type_sorted[index] == attend_type_sorted[index+1]:
                                del attend_type_sorted[index]
                                del attend_list_sorted[index]
                            elif attend_type_sorted[index] != attend_type_sorted[index+1] and attend_type_sorted[index]=='in':
                                in_out_pairs.append((attend_list_sorted[index],attend_list_sorted[index+1]))
                                del attend_type_sorted[index:index+1]
                                del attend_list_sorted[index:index+1]
                            else:
                                del attend_type_sorted[index]
                                del attend_list_sorted[index]

                    #print in_out_pairs
                    total_att_hours = 0.0
                    for in_,out_ in in_out_pairs:
                        total_att_hours += (out_ - in_).total_seconds()/60.0/60.0


                    if total_att_hours >= working_hours_on_day:
                        attendances['number_of_days'] += 1.0
                        attendances['number_of_hours'] += working_hours_on_day

                    else:
                        attendances['number_of_days'] += float(total_att_hours)/float(working_hours_on_day)
                        attendances['number_of_hours'] += total_att_hours
                            
            res += [attendances] + [absence_days] + [no_sign_in_line] + [no_sign_out_line]
        return res