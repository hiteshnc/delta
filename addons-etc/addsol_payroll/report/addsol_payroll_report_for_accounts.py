from openerp.osv import osv
from openerp.report import report_sxw
import datetime
import math



class addsol_payroll_report_for_accounts(report_sxw.rml_parse):


    def __init__(self, cr, uid, name, context):
        super(addsol_payroll_report_for_accounts, self).__init__(cr, uid, name, context)
        self.localcontext.update({
            'get_number_of_days': self._get_number_of_days,
        })


    def _get_number_of_days(self, date_from, date_to):
        #Returns a float equals to the timedelta between two dates given as string.
        DATETIME_FORMAT = "%Y-%m-%d"
        from_dt = datetime.datetime.strptime(date_from, DATETIME_FORMAT)
        to_dt = datetime.datetime.strptime(date_to, DATETIME_FORMAT)
        delta = to_dt - from_dt
        diff_day = delta.days + 1
        return diff_day





class wrapped_report_payslip_a(osv.AbstractModel):
    _name = 'report.addsol_payroll.report_payroll_accounts'
    _inherit = 'report.abstract_report'
    _template = 'addsol_payroll.report_payroll_accounts'
    _wrapped_report_class = addsol_payroll_report_for_accounts

