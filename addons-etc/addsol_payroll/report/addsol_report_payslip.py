from openerp.osv import osv
from openerp.report import report_sxw



class addsol_payslip_report(report_sxw.rml_parse):
    

    def __init__(self, cr, uid, name, context):
        super(addsol_payslip_report, self).__init__(cr, uid, name, context)
        self.localcontext.update({
            'totalamount_to_text': self.totalamount_to_text,
        })
        
    
    def totalamount_to_text(self, total):
        s=str(total).split('.')
        afterdot=s[1]
        beforedot=s[0]
        string=''
        value120={0:'',1:'One',2:'Two',3:'Three',4:'Four',5:'Five',6:'Six',7:'Seven',8:'Eight ',9:'Nine ',10:'Ten ',11:'Eleven ',12:'Twelve ',13:'Thirteen ', 14:'Fourteen ',15:'Fifteen ',16:'Sixteen ',17:'Seventeen ',18:'Eighteen ',19:'Nineteen ',20:'Twenty',30:'Thirty',40:'Fourty',50:'Fifty',60:'Sixty',70:'Seventy',80:'Eighty',90:'Ninety'}
        placevalue={1:'Hundred',3:'Thousand',5:'Lakh',7:'Crore',9:'Arab'}
        if len(beforedot)%2==0:
            for l in range(len(beforedot)):
                if l%2==0 and l<(len(beforedot)-3):
                    string=string+beforedot[l]+','
                elif l>=(len(beforedot)-3):
                    string=string+beforedot[l]
                else:
                    string=string+beforedot[l]
            string1=string+'.'+afterdot
        else:
            for l in range(len(beforedot)):
                if l%2==0 and l<(len(beforedot)-3):
                    string=string+beforedot[l]
                elif l>=(len(beforedot)-3):
                    string=string+beforedot[l]
                else:
                    string=string+beforedot[l]+','
            string1=string+'.'+afterdot


        places=string.split(',')
        ln=len(places)
        pval=(ln-2)*2+3
        words=''
        for i in range(ln):
            num=places[i]

            if int(num) in value120:
                if int(num)==0:
                    words=words+''
                elif pval==1:
                    words=words+value120[int(num)] +" "
                else:
                    words=words+value120[int(num)] +" "+placevalue[pval] +" "
            elif len(num)==2:
                if int(num) in value120:
                    words=words+value120[int(num)] +" "+placevalue[pval] +" "
                else:
                    if ln==1 and pval==1:
                        words=words+value120[int(num)-int(num)%10] +"-"+value120[int(num)%10] 
                    else:
                        words=words+value120[int(num)-int(num)%10] +"-"+value120[int(num)%10] +" "+placevalue[pval] +" "
            else:
                if int(num) < 100:
                    if int(num) in value120:
                        lword=value120[int(num)] +" "
                    else:
                        if int(num) in value120:
                            lword=value120[int(num)] +" "+placevalue[pval] +" "
                        else:
                            lword=value120[int(num)-int(num)%10] +"-"+value120[int(num)%10] +" "
                else:
                    h=int(num)/100
                    num=str(int(num)%(h*100))
                    if int(num) in value120:
                        lword=value120[h] +" "+placevalue[pval] +" "+value120[int(num)] +" "
                    else:
                        lword=value120[h] +" "+placevalue[pval] +" "+value120[int(num)-int(num)%10] +"-"+value120[int(num)%10] 
                words=words+lword


            pval-=2

        lwords=''
        if int(afterdot)<99:
            num=afterdot
            if int(num) in value120:
                if int(num)!=0:
                    lwords=value120[int(num)] +" "+' Paise'
            else:
                lwords=' and '+value120[int(num)-int(num)%10] +"-"+value120[int(num)%10] +" "+'Paise'
        words=words+' Rupees'+lwords
        return words
    



class wrapped_report_payslip_a(osv.AbstractModel):
    _name = 'report.addsol_payroll.addsol_report_payslip_p'
    _inherit = 'report.abstract_report'
    _template = 'addsol_payroll.addsol_report_payslip_p'
    _wrapped_report_class = addsol_payslip_report
