{
    'name' : 'Addsol-Payroll',
    'summary': 'Payroll',
    'description' : """
Addition IT Solutions Pvt. Ltd.
=================================
    Contact:
    * website: www.aitspl.com
    * email: info@aitspl.com
    
Features:
---------
    *Modification on PayRoll & Payslip Report
""",

    'author' : 'Addition IT Solutions Pvt. Ltd.',
    
    'category' : 'Addsol mods',
    'version' : '1.0',
    
    'depends' : [
                 'report',
                 'hr_payroll',
                 ],
    
    'data': [
        'addsol_payroll_view.xml',
        'views/report_payroll.xml',
	'views/report_payroll_accounts.xml',
        #'sequence.xml',
        'addsol_report_payroll.xml',
        #'view/report_appoint_view.xml',
    ],
    'demo': [],
    'test': [],
    'installable': True,
    'auto_install': False,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
