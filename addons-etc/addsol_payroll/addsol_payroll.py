from openerp import _, api, fields, models

class addsol_payroll(models.Model):
    
    _inherit = "hr.payslip"
    
    addition = fields.Float("Addition")
    deduction = fields.Float("Deduction")
    reason = fields.Text("Reason for Addition/Deduction")
   
    
