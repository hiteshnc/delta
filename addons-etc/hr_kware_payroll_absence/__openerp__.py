{
	'name' : "KnowledgeWare Payroll Absence",
    'version' : "0.1",
    'author' : "KnowledgeWare Co.",
	'category' : 'Payroll',
	'website' : 'http://www.kware-eg.com',
    'depends' : ['base','hr','hr_kware_payroll','hr_kware_lattness','hr_attendance'],
	'summary': 'Add Absence amounts to Payslip',
    'description':"""
HR Absence
=====================================
	* Add Absence amounts to Payslip.
	""",
	'data' : ['data/hr_absent_data.xml',
			 'views/absence_rule_view.xml'],
	'installable': True,


}