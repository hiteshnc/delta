# -*- coding: utf-8 -*-
###############################################################################
#    License, author and contributors information in:                         #
#    __openerp__.py file at the root folder of this module.                   #
###############################################################################

from openerp import models, fields, api
from openerp.tools.translate import _
from logging import getLogger


_logger = getLogger(__name__)


class Absence_rule(models.Model):
    

    _inherit = 'hr.delay.structure'

    # no_sign_in_rule = fields.Boolean(
    #     string='No Sign In Rule',
    #     required=False,
    #     readonly=False,
    #     index=False,
    #     default=False,
    #     help=False
    # )
    # no_sign_out_rule = fields.Boolean(
    #     string='No Sign In Rule',
    #     required=False,
    #     readonly=False,
    #     index=False,
    #     default=False,
    #     help=False
    # )
    no_sign_in_rule_rate = fields.Float(
        string='No Sign In Rate',
        required=False,
        readonly=False,
        index=False,
        default=1.0,
        digits=(16, 2),
        help=False
    )
    no_sign_out_rule_rate = fields.Float(
        string='No Sign Out Rate',
        required=False,
        readonly=False,
        index=False,
        default=1.0,
        digits=(16, 2),
        help=False
    )
    absence_rule_rate = fields.Float(
        string='Absence Rate',
        required=False,
        readonly=False,
        index=False,
        default=1.0,
        digits=(16, 2),
        help=False
    )
    