# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016-2017 Addition IT Solutions Pvt. Ltd. (<http://www.aitspl.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    'name' : 'Addsol-Monthly Data',
    'summary': 'SuperStockist stock statement, sales and collection, product sale',
    'description' : """
Addition IT Solutions Pvt. Ltd.
=================================
    Contact:
    * website: www.aitspl.com
    * email: info@aitspl.com
    
Features:
---------
    * SuperStockist monthly closing stock statement
    * SuperStockist sales and collection
    * Product regionwise sale
    * Region wise sales projection
    * POB
""",

    'author' : 'Addition IT Solutions Pvt. Ltd.',
    
    'category' : 'Addsol mods',
    'version' : '1.0',
    
    'depends' : ['base', 'addsol_region_state', 'product'],
    
    'data': [
        #'security/addsol_dr_security.xml',
        #'security/ir.model.access.csv',
        'addsol_monthly_data_view.xml',
    ],
    'demo': [],
    'test': [],
    'installable': True,
    'auto_install': False,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
