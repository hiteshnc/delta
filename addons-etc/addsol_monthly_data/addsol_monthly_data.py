# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016-2017 Addition IT Solutions Pvt. Ltd. (<http://www.aitspl.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models, fields, api, _


class addsol_ss_stock_statement(models.Model):
	_name = 'addsol.ss.stock.statement'
	description = "Superstockist monthly closing stock statement"
	_rec_name = 'asm_user_id'
	
	asm_user_id = fields.Many2one('res.users', "BDM")
	superstockist_id = fields.Many2one('res.partner', "Superstockist")
	statement_date = fields.Date("Date")
	status = fields.Selection([('draft', 'Draft'), ('submitted', 'Submitted'), ('approved', 'Approved')], "Status", readonly=True)
	addsol_ss_stock_statement_ids = fields.One2many('addsol.ss.stock.statement.line', 'addsol_ss_stock_statement_id', "Statement")
	
	_defaults = {
      'status': 'draft'
     }
	
	
class addsol_ss_stock_statement_line(models.Model):
	_name = 'addsol.ss.stock.statement.line'
	
	addsol_ss_stock_statement_id = fields.Many2one('addsol.ss.stock.statement', ondelete='cascade')
	product_id = fields.Many2one('product.template', "Product")
	mrp = fields.Float("MRP")
	opening_stock = fields.Integer("Opening Stock", readonly=True)
	opening_value = fields.Float("Opening Value", readonly=True)
	purchase_quantity = fields.Integer("Purchase Quantity")
	purchase_quantity_scheme = fields.Integer("Purchase Scheme Quantity")
	purchase_value = fields.Float("Purchase Value", readonly=True)
	market_return_quantity = fields.Integer("Market Return Quantity")
	market_return_quantity_scheme = fields.Integer("Market Return Quantity Scheme")
	market_return_value = fields.Float("Market Return Value", readonly=True)
	sales_quantity = fields.Integer("Sales Quantity")
	sales_quantity_scheme = fields.Integer("Sales Quantity Scheme")
	sales_value = fields.Float("Sales Value", readonly=True)
	stock_return_quantity = fields.Integer("Stock Return Quantity")
	stock_return_quantity_scheme = fields.Integer("Stock Return Quantity Scheme")
	stock_return_value = fields.Float("Stock Return Value", readonly=True)
	closing_stock = fields.Integer("Closing Stock", readonly=True)
	closing_value = fields.Float("Closing Value", readonly=True)
	price_to_distributor_net_rate = fields.Float("Price to Distributor(10%) net rate")
	super_stockist_net_rate = fields.Float("Superstockist 6% net rate")
	
	
class addsol_ss_sales_collection(models.Model):
	_name = 'addsol.ss.sales.collection'
	description = "Superstockist monthly sales and collection"
	
	asm_user_id = fields.Many2one('res.users', "BDM")
	superstockist_id = fields.Many2one('res.partner', "Superstockist")
	sales_collection_date = fields.Date("Date")
	status = fields.Selection([('draft', 'Draft'), ('submitted', 'Submitted'), ('approved', 'Approved')], "Status", readonly=True)
	addsol_ss_sales_collection_ids = fields.One2many('addsol.ss.sales.collection.line', 'addsol_ss_sales_collection_id', "Sales and Collection")
	
	_defaults = {
      'status': 'draft'
     }
	
class addsol_ss_sales_collection_line(models.Model):
	_name = 'addsol.ss.sales.collection.line'
	
	addsol_ss_sales_collection_id = fields.Many2one('addsol.ss.sales.collection', ondelete='cascade')
	partner_id = fields.Many2one('res.partner', "Customer")
	region_id = fields.Many2one('addsol.region.state', "HQ")
	mr_user_id = fields.Many2one('res.users', "MR")
	ss_sales = fields.Float("Sales", default=0)
	ss_collection = fields.Float("Colletion", default=0)
	ss_return = fields.Float("Return", default=0)
        is_stockist = fields.Boolean("Is Stockist", default=True)
	
    
class addsol_product_regionwise_sale(models.Model):
    _name = 'addsol.product.regionwise.sale'
    
    asm_user_id = fields.Many2one('res.users', "BDM")
    record_date = fields.Date("Date")
    status = fields.Selection([('draft', 'Draft'), ('submitted', 'Submitted'), ('approved', 'Approved')], "Status", readonly=True)
    addsol_product_regionwise_sale_ids = fields.One2many('addsol.product.regionwise.sale.line', 'addsol_product_regionwise_sale_id')
    
    _defaults = {
      'status': 'draft'
     }
     
class addsol_product_regionwise_sale_line(models.Model):
    _name = 'addsol.product.regionwise.sale.line'
    
    addsol_product_regionwise_sale_id = fields.Many2one('addsol.product.regionwise.sale', ondelete='cascade')
    product_id = fields.Many2one('product.template', "Product")
    mrp = fields.Float("MRP")
    stockist_net_rate = fields.Float("Price to Distributor(10%) net rate")
    region_id = fields.Many2one('addsol.region.state', "HQ")
    quantity = fields.Float("Sales Quantity", default=0)
    sale_amount = fields.Float("Sale Amount", default=0)
    closing_quantity = fields.Integer("Closing Quantity", default=0)
    closing_amount = fields.Float("Closing Amount", default=0)
    

class addsol_regionwise_sale_projection(models.Model):
    _name = 'addsol.regionwise.sale.projection'
    
    asm_user_id = fields.Many2one('res.users', "BDM")
    record_date = fields.Date("Date")
    state = fields.Selection([('draft', 'Draft'), ('submitted', 'Submitted'), ('approved', 'Approved')], "Status", readonly=True)
    addsol_regionwise_sale_projection_ids = fields.One2many('addsol.regionwise.sale.projection.line', 'addsol_regionwise_sale_projection_id')
    
    _defaults = {
      'state': 'draft'
     }
     
class addsol_regionwise_sale_projection_line(models.Model):
    _name = 'addsol.regionwise.sale.projection.line'
    
    addsol_regionwise_sale_projection_id = fields.Many2one('addsol.regionwise.sale.projection', ondelete='cascade')
    product_id = fields.Many2one('product.template', "Product")
    mrp = fields.Float("MRP")
    stockist_net_rate = fields.Float("Price to Distributor(10%) net rate")
    region_id = fields.Many2one('addsol.region.state', "HQ")
    quantity = fields.Float("Quantity")    
  
class addsol_pob(models.Model):
    _name = 'addsol.pob'
	
    asm_user_id = fields.Many2one('res.users', "BDM")
    record_date = fields.Date("Date")
    addsol_pob_lines_ids = fields.One2many('addsol.pob.lines', 'pob_id')
	

class addsol_pob_lines(models.Model):
    _name = 'addsol.pob.lines'
	
    pob_id = fields.Many2one('addsol.pob', ondelete='cascade')
    mr_id = fields.Many2one('res.users', "MR", required=True)
    hq_id = fields.Many2one('addsol.region.state',"Headquater")
    doctor_meet = fields.Integer("Doctors Meet")
    productive_doctor_meet = fields.Integer("Productive Doctor Meet")
    locality = fields.Char("Locality")
    total_doctor_pob = fields.Integer("Total Doctor POB")
    total_chemist_pob = fields.Integer("Total Chemist POB")
    amt = fields.Integer("Amount")
    status = fields.Selection([
                                ('present','Present'),
				('leave','Leave'),
				('meeting','Meeting'),
				('transit','Transit'),
				('non_reporting','Non Reporting'),
				('sunday','Sunday'),
				('holiday','Holiday')
				], "Status" )
    
