
import time
from datetime import datetime,timedelta
from openerp.osv import fields, osv
from openerp import tools
from openerp.tools.translate import _
class hr_payslip_kware(osv.Model):
    _inherit = 'hr.payslip'

    def onchange_employee_id(self, cr, uid, ids, date_from, date_to, employee_id=False, contract_id=False, context=None):
        #print 'in'
        empolyee_obj = self.pool.get('hr.employee')
        contract_obj = self.pool.get('hr.contract')
        worked_days_obj = self.pool.get('hr.payslip.worked_days')
        input_obj = self.pool.get('hr.payslip.input')

        if context is None:
            context = {}
        #delete old worked days ids
        worked_days_ids_to_remove=[]
        old_worked_days_ids = ids and worked_days_obj.search(cr, uid, [('payslip_id', '=', ids[0])], context=context) or False
        if old_worked_days_ids:
            worked_days_ids_to_remove = map(lambda x: (2, x,),old_worked_days_ids)

        #delete old input lines
        input_line_ids_to_remove=[]
        old_input_ids = ids and input_obj.search(cr, uid, [('payslip_id', '=', ids[0])], context=context) or False
        if old_input_ids:
            input_line_ids_to_remove = map(lambda x: (2,x,), old_input_ids)


        #defaults
        res = {'value':{
                      'line_ids':[],
                      'input_line_ids': input_line_ids_to_remove,
                      'worked_days_line_ids': worked_days_ids_to_remove,
                      'name':'',
                      'contract_id': False,
                      'struct_id': False,
                      }
            }

        if (not employee_id) or (not date_from) or (not date_to):
            return res
        ttyme = datetime.fromtimestamp(time.mktime(time.strptime(date_from, "%Y-%m-%d")))
        employee_id = empolyee_obj.browse(cr, uid, employee_id, context=context)
        res['value'].update({
                    'name': _('Salary Slip of %s for %s') % (employee_id.name, tools.ustr(ttyme.strftime('%B-%Y'))),
                    'company_id': employee_id.company_id.id
        })

        if not context.get('contract', False):
            #fill with the first contract of the employee
            contract_ids = self.get_contract(cr, uid, employee_id, date_from, date_to, context=context)
        else:
            if contract_id:
                #set the list of contract for which the input have to be filled
                contract_ids = [contract_id]
            else:
                #if we don't give the contract, then the input to fill should be for all current contracts of the employee
                contract_ids = self.get_contract(cr, uid, employee_id, date_from, date_to, context=context)

        if not contract_ids:
            return res
        contract_record = contract_obj.browse(cr, uid, contract_ids[0], context=context)
        res['value'].update({
                    'contract_id': contract_record and contract_record.id or False
        })
        struct_record = contract_record and contract_record.struct_id or False
        if not struct_record:
            return res
        res['value'].update({
                    'struct_id': struct_record.id,
        })

        #computation of the salary input
        worked_days_line_ids = self.get_worked_day_lines(cr, uid, contract_ids, date_from, date_to, context=context)
        input_line_ids = self.get_inputs(cr, uid, contract_ids, date_from, date_to, context=context)
        res['value'].update({
                    'worked_days_line_ids': worked_days_line_ids,
                    'input_line_ids': input_line_ids,
        })
        #print res
        return res


    def get_worked_day_lines(self, cr, uid, contract_ids, date_from, date_to, context=None):
        """
        @param contract_ids: list of contract id
        @return: returns a list of dict containing the input that should be applied for the given contract between date_from and date_to
        """
        res = super(hr_payslip_kware, self).get_worked_day_lines(cr,uid,contract_ids,date_from,date_to,context=context)
        return res
class hr_contract_eg(osv.Model):
    _inherit = 'hr.contract'
    _columns = {
        'basic_salary': fields.float(string='Basic', required=True),
        'variable_salary': fields.float(string='Variable', required=True),
        'company_id': fields.related('employee_id', 'company_id', type='many2one', relation='res.company', string="Company", readonly=True),
        }

    def onchange_employee_id(self, cr, uid, ids, employee_id , context=None):
        res = {}
        if employee_id:
            employee = self.pool.get('hr.employee').browse(cr, uid, employee_id, context=context)
            res['company_id'] = employee.company_id.id
            res['job_id'] = employee.job_id.id
        return {'value': res}