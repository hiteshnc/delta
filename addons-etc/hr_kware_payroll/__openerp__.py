{
	'name' : "KnowledgeWare - Payroll",
    'version' : "0.1",
    'author' : "KnowledgeWare Co.",
	'category' : 'Payroll',
	'website' : 'http://www.kware-eg.com',
    'depends' : ['base','hr','hr_payroll','hr_contract'],
	'summary': 'Basic Enhancement to Payroll',
    'description':"""
Basic Enhancement to Payroll
=====================================""",
	'data' : ['views/kware_payroll_view.xml'],
	'installable': True,

}