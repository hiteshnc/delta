from openerp import _, api, fields, models
from openerp.osv import fields, osv



class leave_mager(models.Model):
    
    _inherit = "hr.holidays"
    
 


    def is_officer_employee(self, cr, uid, ids, *args):
        for leave in self.browse(cr, uid, ids):
            if uid==leave.employee_id.user_id.id:
                raise osv.except_osv(_('Warning!'), _('Can not approve own Leave!'))
            else:
                return self.write(cr, uid, ids, {'state':'validate1', 'manager_id': False})
            

   