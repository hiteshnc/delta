# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name' : 'Web Organizational Chart',
    'version': '1.0',
    'summary': 'Hierarchical Structure of Companys Employee',
    'category': 'Tools',
    'author': 'Serpent Consulting Services Pvt. Ltd.',
    'website': 'http://www.serpentcs.com',
    'data': [
        "views/templates.xml",
        "web_org_chart.xml",
    ],
    'depends' : ['hr'],
    'qweb': ['static/src/xml/web_org_chart.xml'],
    'price': 20,
    'currency': 'EUR',
}
